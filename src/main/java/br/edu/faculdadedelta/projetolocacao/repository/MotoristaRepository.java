package br.edu.faculdadedelta.projetolocacao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.faculdadedelta.projetolocacao.model.Motorista;

public interface MotoristaRepository extends JpaRepository<Motorista, Long>{

}
