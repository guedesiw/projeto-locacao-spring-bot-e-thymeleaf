package br.edu.faculdadedelta.projetolocacao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.faculdadedelta.projetolocacao.model.Fabricante;

public interface FabricanteRepository extends JpaRepository<Fabricante, Long>{

}
