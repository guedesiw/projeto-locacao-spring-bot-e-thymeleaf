package br.edu.faculdadedelta.projetolocacao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.faculdadedelta.projetolocacao.model.Carro;

public interface CarroRepository extends JpaRepository<Carro, Long>{

}
