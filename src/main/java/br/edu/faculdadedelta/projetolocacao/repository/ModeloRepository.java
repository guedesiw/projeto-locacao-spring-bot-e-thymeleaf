package br.edu.faculdadedelta.projetolocacao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.faculdadedelta.projetolocacao.model.Modelo;

public interface ModeloRepository extends JpaRepository<Modelo, Long>{

}
