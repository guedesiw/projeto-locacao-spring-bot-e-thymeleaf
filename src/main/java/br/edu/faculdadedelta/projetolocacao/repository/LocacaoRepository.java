package br.edu.faculdadedelta.projetolocacao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.faculdadedelta.projetolocacao.model.Locacao;

public interface LocacaoRepository extends JpaRepository<Locacao, Long>{

}
