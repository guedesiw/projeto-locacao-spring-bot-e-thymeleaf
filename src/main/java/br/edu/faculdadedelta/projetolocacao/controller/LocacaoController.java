package br.edu.faculdadedelta.projetolocacao.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.faculdadedelta.projetolocacao.model.Carro;
import br.edu.faculdadedelta.projetolocacao.model.Fabricante;
import br.edu.faculdadedelta.projetolocacao.model.Locacao;
import br.edu.faculdadedelta.projetolocacao.model.Motorista;
import br.edu.faculdadedelta.projetolocacao.repository.CarroRepository;
import br.edu.faculdadedelta.projetolocacao.repository.FabricanteRepository;
import br.edu.faculdadedelta.projetolocacao.repository.LocacaoRepository;
import br.edu.faculdadedelta.projetolocacao.repository.MotoristaRepository;

@Controller
@RequestMapping("/locacoes")
public class LocacaoController {
	
	private static final String LOCACAO_CADASTRO = "locacaoCadastro";
	private static final String LOCACAO_LISTA = "locacaoLista";
	
	@Autowired
	private LocacaoRepository locacaoRepository;
	
	@Autowired
	private CarroRepository carroRepository;
	
	@Autowired
	private MotoristaRepository motoristaRepository;
	
	@RequestMapping("/novo")
	public ModelAndView novo() {// Por padrão é .html
		ModelAndView modelAndView = new ModelAndView(LOCACAO_CADASTRO);
		modelAndView.addObject(new Locacao());
		return modelAndView;
	}
	
	@ModelAttribute(name = "todasLocacoes")
	public List<Locacao> todasLocacoes(){
		return locacaoRepository.findAll();
	}
	
	@ModelAttribute(name = "todosCarros")
	public List<Carro> todosCarros(){
		return carroRepository.findAll();
	}
	
	@ModelAttribute(name = "todosMotoristas")
	public List<Motorista> todosMotoristas(){
		return motoristaRepository.findAll();
	}
	
	@PostMapping
	public ModelAndView salvar(@Valid Locacao locacao, Errors errors, RedirectAttributes redirectAttributes) {
		if(errors.hasErrors()) {//Automaticamente leva o erro para a tela.
			return new ModelAndView(LOCACAO_CADASTRO);
		}
		if(locacao.getId() == null) {
			locacaoRepository.save(locacao);
			redirectAttributes.addFlashAttribute("mensagem", "Inclusão realizada com sucesso!");
		}else {
			locacaoRepository.save(locacao);
			redirectAttributes.addFlashAttribute("mensagem", "Locação alterada com sucesso!");
		}
		return new ModelAndView("redirect:/locacoes");
	}
	
	@GetMapping
	public ModelAndView listar() {
		ModelAndView modelAndView = new ModelAndView(LOCACAO_LISTA);
		modelAndView.addObject("locacoes", todasLocacoes());
		return modelAndView;
	}
	
	@GetMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView(LOCACAO_CADASTRO);
		modelAndView.addObject(locacaoRepository.findById(id).orElseThrow(() -> new EmptyResultDataAccessException(0)));
		return modelAndView;
	}
	
	@GetMapping("/excluir/{id}")
	public ModelAndView excluir(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("redirect:/locacoes");
		locacaoRepository.deleteById(id);
		return modelAndView;
	}
	
}

