package br.edu.faculdadedelta.projetolocacao.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.faculdadedelta.projetolocacao.model.Fabricante;
import br.edu.faculdadedelta.projetolocacao.model.Modelo;
import br.edu.faculdadedelta.projetolocacao.repository.FabricanteRepository;
import br.edu.faculdadedelta.projetolocacao.repository.ModeloRepository;
import br.edu.faculdadedelta.projetolocacao.type.Categoria;

@Controller
@RequestMapping("/modelos")
public class ModeloController {
	
	private static final String MODELO_CADASTRO = "modeloCadastro";
	private static final String MODELO_LISTA = "modeloLista";
	
	@Autowired
	private ModeloRepository modeloRepository;
	
	@Autowired
	private FabricanteRepository fabricanteRepository;
	
	@RequestMapping("/novo")
	public ModelAndView novo() {// Por padrão é .html
		ModelAndView modelAndView = new ModelAndView(MODELO_CADASTRO);
		modelAndView.addObject(new Modelo());
		return modelAndView;
	}
	
	@ModelAttribute(name = "todosModelos")
	public List<Modelo> todosModelos(){
		return modeloRepository.findAll();
	}
	
	@ModelAttribute(name = "todosFabricantes")
	public List<Fabricante> todosFabricantes(){
		return fabricanteRepository.findAll();
	}
	
	@ModelAttribute(name = "todasCategorias")
	public Categoria[] todasCategorias(){
		return Categoria.values();
	}
	
	@PostMapping
	public ModelAndView salvar(@Valid Modelo modelo, Errors errors, RedirectAttributes redirectAttributes) {
		if(errors.hasErrors()) {//Automaticamente leva o erro para a tela.
			return new ModelAndView(MODELO_CADASTRO);
		}
		if(modelo.getId() == null) {
			modeloRepository.save(modelo);
			redirectAttributes.addFlashAttribute("mensagem", "Inclusão realizada com sucesso!");
		}else {
			modeloRepository.save(modelo);
			redirectAttributes.addFlashAttribute("mensagem", "Modelo alterado com sucesso!");
		}
		return new ModelAndView("redirect:/modelos");
	}
	
	@GetMapping
	public ModelAndView listar() {
		ModelAndView modelAndView = new ModelAndView(MODELO_LISTA);
		modelAndView.addObject("modelos", todosModelos());
		return modelAndView;
	}
	
	@GetMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView(MODELO_CADASTRO);
		modelAndView.addObject(modeloRepository.findById(id).orElseThrow(() -> new EmptyResultDataAccessException(0)));
		return modelAndView;
	}
	
	@GetMapping("/excluir/{id}")
	public ModelAndView excluir(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("redirect:/modelos");
		modeloRepository.deleteById(id);
		return modelAndView;
	}
	
}

