package br.edu.faculdadedelta.projetolocacao.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.faculdadedelta.projetolocacao.model.Carro;
import br.edu.faculdadedelta.projetolocacao.model.Modelo;
import br.edu.faculdadedelta.projetolocacao.repository.CarroRepository;
import br.edu.faculdadedelta.projetolocacao.repository.ModeloRepository;

@Controller
@RequestMapping("/carros")
public class CarroController {
	
	private static final String CARRO_CADASTRO = "carroCadastro";
	private static final String CARRO_LISTA = "carroLista";
	
	@Autowired
	private CarroRepository carroRepository;
	
	@Autowired
	private ModeloRepository modeloRepository;
	
	@RequestMapping("/novo")
	public ModelAndView novo() {// Por padrão é .html
		ModelAndView modelAndView = new ModelAndView(CARRO_CADASTRO);
		modelAndView.addObject(new Carro());
		return modelAndView;
	}
	
	@ModelAttribute(name = "todosCarros")
	public List<Carro> todosCarros(){
		return carroRepository.findAll();
	}
	
	@ModelAttribute(name = "todosModelos")
	public List<Modelo> todosModelos(){
		return modeloRepository.findAll();
	}
	
	@PostMapping
	public ModelAndView salvar(@Valid Carro carro, Errors errors, RedirectAttributes redirectAttributes) {
		if(errors.hasErrors()) {//Automaticamente leva o erro para a tela.
			return new ModelAndView(CARRO_CADASTRO);
		}
		if(carro.getId() == null) {
			carroRepository.save(carro);
			redirectAttributes.addFlashAttribute("mensagem", "Inclusão realizada com sucesso!");
		}else {
			carroRepository.save(carro);
			redirectAttributes.addFlashAttribute("mensagem", "Carro alterado com sucesso!");
		}
		return new ModelAndView("redirect:/carros");
	}
	
	@GetMapping
	public ModelAndView listar() {
		ModelAndView modelAndView = new ModelAndView(CARRO_LISTA);
		modelAndView.addObject("carros", todosCarros());
		return modelAndView;
	}
	
	@GetMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView(CARRO_CADASTRO);
		modelAndView.addObject(carroRepository.findById(id).orElseThrow(() -> new EmptyResultDataAccessException(0)));
		return modelAndView;
	}
	
	@GetMapping("/excluir/{id}")
	public ModelAndView excluir(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("redirect:/carros");
		carroRepository.deleteById(id);
		return modelAndView;
	}
	
}

