package br.edu.faculdadedelta.projetolocacao.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.faculdadedelta.projetolocacao.model.Motorista;
import br.edu.faculdadedelta.projetolocacao.repository.MotoristaRepository;
import br.edu.faculdadedelta.projetolocacao.type.Sexo;

@Controller
@RequestMapping("/motoristas")
public class MotoristaController {
	
	private static final String MOTORISTA_CADASTRO = "motoristaCadastro";
	private static final String MOTORISTA_LISTA = "motoristaLista";
	
	@Autowired
	private MotoristaRepository motoristaRepository;
	
	@RequestMapping("/novo")
	public ModelAndView novo() {// Por padrão é .html
		ModelAndView modelAndView = new ModelAndView(MOTORISTA_CADASTRO);
		modelAndView.addObject(new Motorista());
		return modelAndView;
	}
	
	@ModelAttribute(name = "todosMotoristas")
	public List<Motorista> todosMotoristas(){
		return motoristaRepository.findAll();
	}
	
	@ModelAttribute(name = "todosSexos")
	public Sexo[] todosSexos(){
		return Sexo.values();
	}
	
	@PostMapping
	public ModelAndView salvar(@Valid Motorista motorista, Errors errors, RedirectAttributes redirectAttributes) {
		if(errors.hasErrors()) {//Automaticamente leva o erro para a tela.
			return new ModelAndView(MOTORISTA_CADASTRO);
		}
		if(motorista.getId() == null) {
			motoristaRepository.save(motorista);
			redirectAttributes.addFlashAttribute("mensagem", "Inclusão realizada com sucesso!");
		}else {
			motoristaRepository.save(motorista);
			redirectAttributes.addFlashAttribute("mensagem", "Motorista alterado com sucesso!");
		}
		return new ModelAndView("redirect:/motoristas");
	}
	
	@GetMapping
	public ModelAndView listar() {
		ModelAndView modelAndView = new ModelAndView(MOTORISTA_LISTA);
		modelAndView.addObject("motoristas", todosMotoristas());
		return modelAndView;
	}
	
	@GetMapping("/editar/{id}")
	public ModelAndView editar(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView(MOTORISTA_CADASTRO);
		modelAndView.addObject(motoristaRepository.findById(id).orElseThrow(() -> new EmptyResultDataAccessException(0)));
		return modelAndView;
	}
	
	@GetMapping("/excluir/{id}")
	public ModelAndView excluir(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("redirect:/motoristas");
		motoristaRepository.deleteById(id);
		return modelAndView;
	}
	
}

