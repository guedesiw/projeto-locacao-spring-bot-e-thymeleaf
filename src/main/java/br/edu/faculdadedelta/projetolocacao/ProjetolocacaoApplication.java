package br.edu.faculdadedelta.projetolocacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetolocacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetolocacaoApplication.class, args);
	}

}
